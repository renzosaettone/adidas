# adidas

This Project include:
Review Microservice
Product Microservice
Dockerfile to run
Postman Collection for Test

## How to Run
1.Clone the repository
2.In root folder run: docker-compose up -d --build

### EndPoints and Ports
Review Microservice run on port 3000
Product Microservice run on port 4000
You can use the Postman Collection to Test the APIs

#### IMPORTANT: Credentials for the service: adminAdidas / hereToCreate