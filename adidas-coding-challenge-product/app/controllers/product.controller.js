var request = require('request-promise');
var async = require('async');

exports.getProduct = (req, res) => {
    async.parallel({
        product: function(callback) {
            // Call to Product Service
            request("https://www.adidas.co.uk/api/products/" + req.params.productId, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    callback(null, body);
                } else {
                  callback(true, {});
                }
            });
          },
          review: function(callback) {
            // Call to Review Service
            request("http://review-service:3000/reviews/" + req.params.productId, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    callback(null, body);
                } else {
                  callback(true, {});
                }
            });
          }
        }, function(err, results) {
            var aggregation = JSON.parse(results.product);
            aggregation.reviews = JSON.parse(results.review);
          res.writeHead(200, {"Content-Type": "application/json"});
          res.end(JSON.stringify(aggregation));
        });
}