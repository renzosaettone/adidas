module.exports = (app) => {
    const products = require('../controllers/product.controller.js');
   
    // Retrieve a single Product with a productId
    app.get('/products/:productId', products.getProduct);

}
