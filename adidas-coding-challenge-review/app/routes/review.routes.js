module.exports = (app) => {
    const reviews = require('../controllers/review.controller.js');

    // Retrieve all Reviews
    app.get('/reviews', reviews.findAll);
    
    // Retrieve a single Review with a productId
    app.get('/reviews/:productId', reviews.findOne);

    // Create a new Review
    app.post('/reviews', reviews.create);

    // Update a Review with productId
    app.put('/reviews/:productId', reviews.update);

    // Delete a Review with productId
    app.delete('/reviews/:productId', reviews.delete);

}
