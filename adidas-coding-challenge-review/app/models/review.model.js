const mongoose = require('mongoose');

const ReviewSchema = mongoose.Schema({
    productId: String,
    averageReviewScore: Number,
    numberOfReviews: Number
}, {
    timestamps: true
});

module.exports = mongoose.model('Review', ReviewSchema);