const Review = require('../models/review.model.js');

// Create and Save a new Review
exports.create = (req, res) => {
    // Validate request Authorization
     if(!req.headers.authorization || req.headers.authorization != "Basic YWRtaW5BZGlkYXM6aGVyZVRvQ3JlYXRl") {
        return res.status(401).send({
            message: "Not Authorized or Incorrect Credentials"
        });
    }

    // Validate request
    if(!req.body.productId) {
        return res.status(400).send({
            message: "Review data incomplete, you have to send at least the productId"
        });
    }

    // Create a Review
    const review = new Review({
        productId: req.body.productId,
        averageReviewScore: req.body.averageReviewScore || 0,
        numberOfReviews: req.body.numberOfReviews || 0
    });

    // Save Review in the database
    review.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Review."
        });
    });
};

// Retrieve and return all reviews from the database.
exports.findAll = (req, res) => {
    Review.find()
    .then(reviews => {
        res.send(reviews);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving reviews."
        });
    });
};

// Find a single Review with a productId
exports.findOne = (req, res) => {
    Review.findOne({productId: req.params.productId})
    .then(review => {
        if(!review) {
            return res.status(404).send({
                message: "Review not found with productId " + req.params.productId
            });            
        }
        res.send(review);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Review not found with productId " + req.params.productId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Review with productId " + req.params.productId
        });
    });
};

// Update a Review identified by the productId in the request
exports.update = (req, res) => {

    // Validate request Authorization
    if(!req.headers.authorization || req.headers.authorization != "Basic YWRtaW5BZGlkYXM6aGVyZVRvQ3JlYXRl") {
        return res.status(401).send({
            message: "Not Authorized or Incorrect Credentials"
        });
    }

    // Validate Request
    if(!req.params.productId || !req.body.averageReviewScore || !req.body.numberOfReviews) {
        return res.status(400).send({
            message: "Review data incomplete, you have to send productId in the path i.e reviews/ABC123 and averageReviewScore, numberOfReviews in the body"
        });
    }

    // Find Review and update it with the request body
    Review.findOneAndUpdate({productId: req.params.productId}, {
        productId: req.params.productId,
        averageReviewScore: req.body.averageReviewScore,
        numberOfReviews: req.body.numberOfReviews
    }, {new: true})
    .then(review => {
        if(!review) {
            return res.status(404).send({
                message: "Review not found with productId " + req.params.productId
            });
        }
        res.send(review);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Review not found with productId " + req.params.productId
            });                
        }
        return res.status(500).send({
            message: "Error updating Review with productId " + req.params.productId
        });
    });
};

// Delete a Review with the specified productId in the request
exports.delete = (req, res) => {

    // Validate request Authorization
    if(!req.headers.authorization || req.headers.authorization != "Basic YWRtaW5BZGlkYXM6aGVyZVRvQ3JlYXRl") {
        return res.status(401).send({
            message: "Not Authorized or Incorrect Credentials"
        });
    }

    Review.findOneAndRemove({productId: req.params.productId})
    .then(review => {
        if(!review) {
            return res.status(404).send({
                message: "Review not found with productId " + req.params.productId
            });
        }
        res.send({message: "Review deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Review not found with productId " + req.params.productId
            });                
        }
        return res.status(500).send({
            message: "Could not delete Review with productId " + req.params.productId
        });
    });
};